﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Support.V7.Widget;
using Android.Content;

namespace PruebaRestaurant
{
    [Activity(Label = "Prueba Restaurant", Theme = "@android:style/Theme.Material.Light.NoActionBar",
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Icon = "@android:color/transparent")]
    public class MainActivity : Activity
    {
        private CardView btnMenu;
        private CardView btnPago;
        private CardView btnReservacion;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
            Init();
        }

        private void Init()
        {
            btnMenu = FindViewById<CardView>(Resource.Id.cvMenu);
            btnPago = FindViewById<CardView>(Resource.Id.cvPago);
            btnReservacion = FindViewById<CardView>(Resource.Id.cvReservacion);
        }

        protected override void OnResume()
        {
            base.OnResume();
            btnReservacion.Click += CambiarActivity;
            btnMenu.Click += CambiarActivity;
            btnPago.Click += CambiarActivity;
        }

        private void CambiarActivity(object sender, EventArgs args)
        {
            CardView boton = sender as CardView;

            switch(boton.Id)
            {
                case Resource.Id.cvMenu:
                    StartActivity(new Intent(this, typeof(Categorias)));
                    break;
                case Resource.Id.cvPago:
                    break;
                case Resource.Id.cvReservacion:
                    break;
            }
        }
    }
}

