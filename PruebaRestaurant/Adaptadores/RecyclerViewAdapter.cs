﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace PruebaRestaurant.Adaptadores
{
    public class RecyclerViewHolder : RecyclerView.ViewHolder
    {
        /// <summary>
        /// Campos del Card Template
        /// </summary>
        private ImageView ivIcono;
        private TextView tvCategoria;
        private TextView tvPlatillo;

        public ImageView IvIcono { get => ivIcono; set => ivIcono = value; }
        public TextView TvCategoria { get => tvCategoria; set => tvCategoria = value; }
        public TextView TvPlatillo { get => tvPlatillo; set => tvPlatillo = value; }
        /// <summary>
        /// Inicializamos las variables enlazando los controles
        /// </summary>
        /// <param name="itemView"> View para enlazar los cotntroles</param>
        /// <param name="listener"> Listener para maneñar los eventos</param>
        public RecyclerViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            IvIcono = itemView.FindViewById<ImageView>(Resource.Id.ivImagenAlimento);
            TvCategoria = itemView.FindViewById<TextView>(Resource.Id.tvCategoriaItem);
            TvPlatillo = itemView.FindViewById<TextView>(Resource.Id.tvNombreAlimento);
            itemView.Click += (sender, e) => listener(LayoutPosition);
        }
    }

    public class RecyclerViewAdapter : RecyclerView.Adapter
    {
        
        private List<InformacionCard> dataSet;//Informacion para llenar los cards
        private Context context; //Contexto en que se ejecuta
        public event EventHandler<InformacionCard> ItemClick; //Event handler

        /// <summary>
        /// Handler para los eventos
        /// </summary>
        /// <param name="posicion"> posicion de item en el recycler view</param>
        void OnClick(int posicion)
        {
            ItemClick?.Invoke(this, dataSet[posicion]);
        }

        /// <summary>
        /// Contructor para inicializar las variables
        /// </summary>
        /// <param name="dataSet">Informacion para el card</param>
        /// <param name="context">Contexto de la aplicacion</param>
        public RecyclerViewAdapter(List<InformacionCard> dataSet, Context context)
        {
            this.dataSet = dataSet;
            this.context = context;
        }

        /// <summary>
        /// COntador del dataSet
        /// </summary>
        public override int ItemCount => dataSet.Count;

        /// <summary>
        /// Enlaza los datos del dataset al card view
        /// </summary>
        /// <param name="holder"> enlace </param>
        /// <param name="position"> posicion del item </param>
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            RecyclerViewHolder recyclerHolder  = holder as RecyclerViewHolder;
            recyclerHolder.IvIcono.SetImageResource(dataSet[position].ImagenId);
            recyclerHolder.TvPlatillo.Text = dataSet[position].Platillo;
            recyclerHolder.TvCategoria.Text = dataSet[position].Categoria;
        }

        /// <summary>
        /// Agrega los elementos a la View de la Activity Correspondiente
        /// </summary>
        /// <param name="parent"> Activity que invoca</param>
        /// <param name="viewType"></param>
        /// <returns>View Holder</returns>
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            LayoutInflater inflater = LayoutInflater.From(parent.Context);
            View card = inflater.Inflate(Resource.Layout.CardTemplate,parent,false);
            return new RecyclerViewHolder(card,OnClick);
        }
    }
}