﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace PruebaRestaurant.Adaptadores
{
    public class InformacionCard
    {
        public int ImagenId
        {
            get;
            set;
        }
        public string Categoria
        {
            get;
            set;
        }
        public string Platillo
        {
            get;
            set;
        }
    }
}