﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace PruebaRestaurant
{
    [Activity(Label = "Restaurant", MainLauncher = true, Theme = "@android:style/Theme.Material.Light.NoActionBar", NoHistory = true, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class SplashScreen : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SpashScreen);
        }
        /// <summary>
        /// Iniciamos la Splash Screen
        /// </summary>
        protected override void OnResume()
        {
            base.OnResume();
            Task startupWork = new Task(() => { SimulateStartup(); });
            startupWork.Start();
        }

        /// <summary>
        /// Evitamos cancelar el evento del back button 
        /// </summary>
        public override void OnBackPressed() { }

        /// <summary>
        /// Delay del Splash Screen 
        /// </summary>
        async void SimulateStartup()
        {
            await Task.Delay(4000); //Esperamos 8 segundos
            StartActivity(new Intent(Application.Context, typeof(MainActivity)));
            Finish();
        }
    }
}