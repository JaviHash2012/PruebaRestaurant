﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using PruebaRestaurant.Adaptadores;

namespace PruebaRestaurant
{
    [Activity(Label = "Platillos List", Theme = "@android:style/Theme.Material.Light", ParentActivity = typeof(Categorias),
              Icon = "@android:color/transparent", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class PlatillosList : Activity
    {
        private RecyclerView rvPlatillos;
        private string categoria;
        private List<InformacionCard> dataSet;
        private RecyclerViewAdapter adapter;
        private RecyclerView.LayoutManager layoutManager;
        private Intent siguienteActivity;
        private string[] menuDesayunos;
        private string[] menuBebidas;
        private string[] menuComidas;
        private string[] menuPostres;
        private ISharedPreferences preferences;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.PlatillosMenu);
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            ActionBar.SetDisplayShowHomeEnabled(true);
            Init();
        }

        //BACK BUTTON
        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.Back)
            {
                //Intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask);
                this.Finish();
                return true;
            }
            if (keyCode == Keycode.Home)
            {
                //Intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask);
                this.Finish();
                return true;
            }
            return base.OnKeyDown(keyCode, e);
        }
        /// <summary>
        /// Inicializamos los componentes 
        /// </summary>
        private void Init()
        {
            //Obtenemos los SharedPreferences
            preferences = Application.GetSharedPreferences("Categorias",FileCreationMode.Private);
            
            //Enlace entre los array-string de los Resources
            menuDesayunos = Resources.GetStringArray(Resource.Array.desayunos);
            menuPostres = Resources.GetStringArray(Resource.Array.postres);
            menuComidas = Resources.GetStringArray(Resource.Array.comidas);
            menuBebidas = Resources.GetStringArray(Resource.Array.bebidas);
            //Obtenemos la categoria seleccionada
            categoria = preferences.GetString("Categoria","");
            ActionBar.Title = categoria;
            //Enlace para el Recycler view
            //Añadimos un tamaño fijo al Width y Height del Activity
            //Añade un Layout Manager para posicionar los elementos en el Recycler View
            rvPlatillos = FindViewById<RecyclerView>(Resource.Id.recyclerViewAlimentos);
            rvPlatillos.HasFixedSize = true;
            layoutManager = new LinearLayoutManager(this);
            rvPlatillos.SetLayoutManager(layoutManager);
            //LLenado del dataSet
            dataSet = LlenarLista(categoria);
            //Se crea y asigna el adapter personalizado para el recycler view
            adapter = new RecyclerViewAdapter(dataSet, this);
            rvPlatillos.SetAdapter(adapter);
            //Agregamos un listener a cada uno de los platillos en la lista
            adapter.ItemClick += OnItemClick;
        }

        /// <summary>
        /// Cambia al siguiente activity
        /// </summary>
        /// <param name="sender">Invocador</param>
        /// <param name="alimentos">Informacion del invocador</param>
        private void OnItemClick(object sender, InformacionCard alimentos)
        {
            //Se crea el siguiente activity
            siguienteActivity = new Intent(this, typeof(DescripcionPlatillo));
            //Pasamos como parametros el alimento y el Id de la Imagen
            siguienteActivity.PutExtra("Alimento", alimentos.Platillo);
            siguienteActivity.PutExtra("IdImagen", alimentos.ImagenId);
            StartActivity(siguienteActivity);
        }

        /// <summary>
        /// LLenamos el dataset dependiendo de la categoria
        /// </summary>
        /// <param name="categoria">Categoria del menu de alimentos</param>
        /// <returns> Dataset </returns>
        private List<InformacionCard> LlenarLista(string categoria)
        {
            List<InformacionCard> informacion = new List<InformacionCard>();

            switch (categoria)
            {
                case "bebidas":
                    for (int i = 0; i < menuBebidas.Length; i++)
                    {
                        informacion.Add(new InformacionCard
                        {
                            ImagenId = Resource.Drawable.bebidas,
                            Platillo = menuBebidas[i],
                            Categoria = "Bebidas"
                        });
                    }
                break;

                case "comidas":
                    for (int i = 0; i < menuComidas.Length; i++)
                    {
                        informacion.Add(new InformacionCard
                        {
                            ImagenId = Resource.Drawable.comida,
                            Platillo = menuComidas[i],
                            Categoria = "Comidas"
                        });
                    }
                break;

                case "desayunos":
                    for (int i = 0; i < menuDesayunos.Length; i++)
                    {
                        informacion.Add(new InformacionCard
                        {
                            ImagenId = Resource.Drawable.desayuno,
                            Platillo = menuDesayunos[i],
                            Categoria = "Desayunos"
                        });
                    }
                break;

                case "postres":
                    for (int i = 0; i < menuPostres.Length; i++)
                    {
                        informacion.Add(new InformacionCard
                        {
                            ImagenId = Resource.Drawable.postres,
                            Platillo = menuPostres[i],
                            Categoria = "Postres"
                        });
                    }
                break;

            }
            return informacion;
        }
    }
}