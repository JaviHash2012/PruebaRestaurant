﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
namespace PruebaRestaurant
{
    [Activity(Label = "Descripcion Platillo", Theme = "@android:style/Theme.Material.Light", NoHistory = true, 
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, ParentActivity = typeof(PlatillosList),Icon = "@android:color/transparent")]
    public class DescripcionPlatillo : Activity
    {
        private string platillo;
        private int idImagen;
        private ImageView ivPlatillo;
        private TextView tvPlatillo;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Descripcion);
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            ActionBar.SetDisplayShowHomeEnabled(true);
            Init();
        }

        //BACK BUTTON
        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.Back)
            {
                //Intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask);
                this.Finish();
                return true;
            }
            return base.OnKeyDown(keyCode, e);
        }
        /// <summary>
        /// Inicializamos los componentes del view
        /// Obtenemos y establecemos los valores del platillo y de la imagen
        /// </summary>
        private void Init()
        {
            platillo = Intent.GetStringExtra("Alimento");
            idImagen = Intent.GetIntExtra("IdImagen",0);
            ActionBar.Title = platillo;
            ivPlatillo = FindViewById<ImageView>(Resource.Id.ivPlatilloDescripcion);
            ivPlatillo.SetImageResource(idImagen);
            tvPlatillo = FindViewById<TextView>(Resource.Id.tvPlatillo);
            tvPlatillo.Text = platillo;
        }
    }
}