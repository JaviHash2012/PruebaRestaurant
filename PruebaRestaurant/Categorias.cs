﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace PruebaRestaurant
{
    [Activity(Label = "Categorias", Theme = "@android:style/Theme.Material.Light", ParentActivity = typeof(MainActivity), Icon = "@android:color/transparent")]
    public class Categorias : Activity
    {
        private CardView cvBebidas;
        private CardView cvComidas;
        private CardView cvPostres;
        private CardView cvDesayunos;
        private ISharedPreferences preferences;
        private ISharedPreferencesEditor editor;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Categorias);
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            ActionBar.SetHomeButtonEnabled(true);
            Init();
        }

        /// <summary>
        /// Inicializa los componentes
        /// </summary>
        private void Init()
        {
            cvBebidas = FindViewById<CardView>(Resource.Id.cvBebidas);
            cvComidas = FindViewById<CardView>(Resource.Id.cvComidas);
            cvPostres = FindViewById<CardView>(Resource.Id.cvPostres);
            cvDesayunos = FindViewById<CardView>(Resource.Id.cvDesayunos);
            preferences = Application.GetSharedPreferences("Categorias", FileCreationMode.Private);
            editor = preferences.Edit();
        }

        protected override void OnResume()
        {
            base.OnResume();
            cvComidas.Click += CambiarActivity;
            cvDesayunos.Click += CambiarActivity;
            cvPostres.Click += CambiarActivity;
            cvBebidas.Click += CambiarActivity;
        }

        /// <summary>
        /// Cambia de activity a la siguiente
        /// </summary>
        /// <param name="sender"> Invocador </param>
        /// <param name="args"> informacion del invocador </param>
        private void CambiarActivity(object sender, EventArgs args)
        {
            //Hacemos un DownCast para obtener las propiedades del Cardview que invoco el evento
            CardView invocador = sender as CardView;
            Intent siguiente = new Intent(this, typeof(PlatillosList));
            //Depende del invocador pasamos la categoria al siguiente activity
            switch (invocador.Id)
            {
                case Resource.Id.cvBebidas:
                    editor.PutString("Categoria","bebidas");
                    editor.Apply();
                    StartActivity(siguiente);
                    break;
                case Resource.Id.cvComidas:
                    editor.PutString("Categoria", "comidas");
                    editor.Apply();
                    StartActivity(siguiente);
                    break;
                case Resource.Id.cvPostres:
                    editor.PutString("Categoria", "postres");
                    editor.Apply();
                    StartActivity(siguiente);
                    break;
                case Resource.Id.cvDesayunos:
                    editor.PutString("Categoria", "desayunos");
                    editor.Apply();
                    StartActivity(siguiente);
                    break;
            }
        }

        //BACK BUTTON
        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.Back)
            {
                //Intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask);
                this.Finish();
                return true;
            }
            if (keyCode == Keycode.Home)
            {
                //Intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask);
                this.Finish();
                return true;
            }
            return base.OnKeyDown(keyCode, e);
        }
    }
}